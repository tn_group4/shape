/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.shape;

/**
 *
 * @author HP
 */
public class Rectangle extends Shape{
    protected double width,height;
   
    
    public Rectangle(double width, double height){
        System.out.println("Rectangle created");
        this.width = width;
        this.height = height;
    }
    
    public double calArea(){
        return width*height;
    }
    
    public void print(){
        System.out.println("Rectangle: width = " + this.width + "height = " + this.height + " Area = " +calArea());
    }
}

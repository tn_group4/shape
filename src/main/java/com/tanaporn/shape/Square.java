/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tanaporn.shape;

/**
 *
 * @author HP
 */
public class Square extends Rectangle{
    private double side;
    
    public Square(double side){
        super(side, side);
        System.out.println("Square created");
        this.side = side;
    }
    
    public double calArea(){
        return side*side;
    }
    
    public void print(){
        System.out.println("Square : side = " + this.side +" Area = " +calArea());
    }
}
